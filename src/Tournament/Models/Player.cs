﻿namespace Tournament.Models
{
    public interface IPlayer
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        int GamesPlayed { get; set; }
        PlayerPositions Position { get; set; }
        void PlayMatch();
    }

    public class Player : IPlayer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int GamesPlayed { get; set; } = 0;

        public PlayerPositions Position { get; set; }

        public void PlayMatch()
        {
            GamesPlayed++;
        }
    }
}