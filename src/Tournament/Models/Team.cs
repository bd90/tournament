﻿using System;
using System.Collections.Generic;

namespace Tournament.Models
{
    public class Team
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        public ICollection<IPlayer> Players { get; private set; }

        private const int MAX_NUMBER_OF_PLAYERS = 11;
        private const int MIN_NUMBER_OF_PLAYERS_TO_PLAY_A_GAME = 3;

        public Team(int id, string name)
        {
            Id = id;
            Name = name;
            
            Players = new List<IPlayer>();
        }

        public void AddPlayer(IPlayer player)
        {
            if (Players.Count == MAX_NUMBER_OF_PLAYERS) 
                throw new Exception("No of players has reached maximum value");
            
            Players.Add(player);
        }

        public void PlayMatch()
        {
            if (Players.Count <= MIN_NUMBER_OF_PLAYERS_TO_PLAY_A_GAME)
                throw new Exception("No enough players to play a game, ");

            foreach (var player in Players)
            {
                player.PlayMatch();
            }
        }
    }
}