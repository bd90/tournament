﻿using System;
using System.Collections.Generic;
using Tournament.Models;
using Xunit;
using Moq;

namespace Tournament.Tests
{

    #region HelperClasses

    public class TeamBuilder
    {
        public int Id { get; set; } = 1;
        public string Name { get; set; } = "Example Team";
        public int NumberOfPlayers { get; set; } = 0;
        
        public Team Build()
        {
            var team = new Team(Id, Name);

            for (var i = 0; i < NumberOfPlayers; i++)
            {
                team.AddPlayer(new Mock<IPlayer>().Object);
            }

            return team;
        }

        public TeamBuilder WithId(int id)
        {
            Id = id;
            return this;
        }

        public TeamBuilder WithName(string name)
        {
            Name = name;
            return this;
        }

        public TeamBuilder WithNumberOfPlayers(int number)
        {
            NumberOfPlayers = number;
            return this;
        }
    }

    #endregion
    
    public class TeamTests
    {   
        [Fact]
        public void Team_instance_should_have_empty_players_list_after_initialization()
        {
            var team = new TeamBuilder()
                .Build();

            Assert.IsType<List<IPlayer>>(team.Players);
            Assert.Equal(0, team.Players.Count);
        }

        [Fact]
        public void Team_should_be_able_to_get_new_players()
        {
            // Arrange
            var team = new TeamBuilder()
                .Build();
            var player = new Mock<IPlayer>();
            
            // Act
            team.AddPlayer(player.Object);
            
            // Assert
            Assert.Equal(1, team.Players.Count);
        }

        [Fact]
        public void Team_should_rise_an_exception_when_maximum_number_of_players_is_reached()
        {
            var team = new TeamBuilder()
                .WithNumberOfPlayers(11)
                .Build();
            var player = new Mock<IPlayer>();

            Assert.ThrowsAny<Exception>(() =>
            {
                team.AddPlayer(player.Object);
            });
        }

        [Fact]
        public void Team_should_not_be_able_to_play_a_game_with_less_than_3_players()
        {
            var team = new TeamBuilder()
                .Build();

            Assert.ThrowsAny<Exception>(() =>
            {
                team.PlayMatch();
            });
        }

        [Fact]
        public void Team_should_update_players_games_count_after_match()
        {
            var team = new TeamBuilder()
                .WithNumberOfPlayers(7)
                .Build();
            var player = new Mock<IPlayer>();
            team.AddPlayer(player.Object);
            
            team.PlayMatch();
            
            player.Verify(p => p.PlayMatch());
        }
    }
}